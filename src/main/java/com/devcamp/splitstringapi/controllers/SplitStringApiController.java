package com.devcamp.splitstringapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SplitStringApiController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true, name = "string") String requestString){
        ArrayList<String> splitStringArray = new ArrayList<>();

        String[] stringArray = requestString.split(" ");
        for (String stringElement : stringArray){
            splitStringArray.add(stringElement);
        }
        return splitStringArray;
    }
}
